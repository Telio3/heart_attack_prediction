import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from models.MLP_classifier import MLP_classifier_model
from models.logistic_regression import logistic_regression_model
from models.random_forest_classifier import random_forest_classifier_model
from models.svc import SVC_model
import plots

# get data
df = pd.read_csv("data/heart.csv")

np.random.seed(42)

# display plots
plots.number_of_patients_by_output(df)
plots.age_by_output(df)
plots.maximum_heart_rate_achieved_by_age(df)
plots.distribution_of_BP_around_patients(df)
plots.data_relations(df)
plots.age_distribution_of_patients_depending_on_heart_disease(df)

# split data
x = df.iloc[:, 1:-1].values
y = df.iloc[:, -1].values

# split data into train and test
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)

# scale data
scaler = StandardScaler()
x_train = scaler.fit_transform(x_train)
x_test = scaler.fit_transform(x_test)

# train models
logistic_regression_model(x_train, y_train, x_test, y_test)
SVC_model(x_train, y_train, x_test, y_test)
random_forest_classifier_model(x_train, y_train, x_test, y_test)
MLP_classifier_model(x_train, y_train, x_test, y_test)
