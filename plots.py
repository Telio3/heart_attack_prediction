import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from pandas import DataFrame


def number_of_patients_by_output(df: DataFrame):
    plt.figure(figsize=(10, 6))
    sns.countplot(x='output', data=df, palette='RdBu_r')
    plt.title('Number of patients by output')
    plt.ylabel('Number of patients')
    plt.show()


def age_by_output(df: DataFrame):
    plt.figure(figsize=(10, 6))
    sns.boxplot(x='output', y='age', data=df, palette='RdBu_r')
    plt.title('Age by output')
    plt.show()


def maximum_heart_rate_achieved_by_age(df: DataFrame):
    x = df['thalachh']
    y = df['age']
    s = np.random.rand(*x.shape) * 150 + 50

    plt.scatter(x, y, s, c="r", alpha=0.8, marker=r'.', label="Heart Rate")
    plt.xlabel("Maximum heart rate achieved")
    plt.ylabel("AGE")
    plt.legend(loc='lower left')
    plt.title("Maximum heart rate achieved by age")
    plt.show()


def distribution_of_BP_around_patients(df: DataFrame):
    plt.figure(figsize=(10, 6))
    sns.displot(df["trtbps"])
    plt.title("Distribution of BP \n around Patients", y=0.5, x=0.8)
    plt.xlabel("Blood Pressure")
    plt.ylabel("Count")
    plt.show()


def data_relations(df: DataFrame):
    con_columns = ["age", "trtbps", "chol", "thalachh", "oldpeak", "output"]
    sns.pairplot(df[con_columns], hue="output", palette=sns.color_palette(["#000080", "#00ffff"]))
    plt.show()


def age_distribution_of_patients_depending_on_heart_disease(df: DataFrame):
    plt.figure(figsize=(16, 10))
    sns.set_style("white")
    plt.title('Age distribution of patients depending on heart disease', size=20, y=1.03)
    plt.grid(color='gray', linestyle=':', axis='x', alpha=0.8, zorder=0, dashes=(1, 7))
    a = sns.kdeplot(df.loc[(df['output'] == 0), 'age'], color='red', fill=True, label='No heart disease')
    sns.kdeplot(df.loc[(df['output'] == 1), 'age'], color='blue', fill=True, label='Heart disease')
    plt.ylabel('')
    plt.xlabel('')
    plt.xticks(fontname='monospace')
    plt.yticks([])

    for j in ['right', 'left', 'top']:
        a.spines[j].set_visible(False)
    a.spines['bottom'].set_linewidth(1.2)

    plt.figtext(0.65, 0.65, '''mean 56.6''', fontsize=14, color='#1e434c', ha='center')
    plt.figtext(0.45, 0.55, '''mean 52.5''', fontsize=14, color='#8d230f', ha='center')

    plt.show()
