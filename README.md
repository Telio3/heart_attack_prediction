﻿# Prédiction d’attaques cardiaques

CORRE Telio – COLLIN Tom : B3 DEV EPSI Rennes

## 1 – Introduction

Ce rapport a pour objectif d'examiner l'utilisation des algorithmes d'apprentissage en machine learning pour prédire le risque de crise cardiaque. Le jeu de données utilisé pour cette étude provient de [kaggle.com](https://www.kaggle.com/). Il comporte 303 observations, avec 14 variables différentes. L'objectif de cette étude est de comprendre la pertinence des algorithmes sélectionnés pour la prédiction du risque de crise cardiaque, ainsi que de déterminer les forces et les faiblesses de ces algorithmes.

## 2 – Jeu de donnée

Le jeu de données utilisé dans cette étude comporte 303 observations et 14 variables différentes. Les variables incluent :

- l'âge
- le sexe
- la présentation clinique de la douleur thoracique (cp)
- la tension artérielle à repos (trtbps)
- le taux de cholestérol (chol)
- le taux de glucose à jeun (frs)
- l'état électrocardiographique à repos (restecg)
- le nombre de battements cardiaques maximum au cours d'un exercice (thalach)
- la présence d'exercice d'effort angineux (exng)
- la dépression du segment ST par rapport au repos (oldpeak)
- le nombre de vaisseaux colorés par flouroscopie (ca)
- le nombre de vaisseaux affectés par un thrombus (slp)
- le nombre d'interférences électriques observées (thal)
- l'état d'output

Les données ont été collectées à partir de [kaggle.com](https://www.kaggle.com/).

Il est important de noter que ces variables peuvent affecter le risque de crise cardiaque d'une personne. Par conséquent, il est crucial de comprendre comment ces variables interagissent entre elles pour obtenir des prédictions précises.

Ici la variable output prend la valeur 0 dans le cas où le patient présente un risque faible d’avoir une crise cardiaque et 1 si le risque est élevé.

Le jeu de données est divisé en deux parties : un ensemble d'entraînement et un ensemble de test. L'ensemble d'entraînement est utilisé pour entraîner les algorithmes d'apprentissage, tandis que l'ensemble de test est utilisé pour évaluer la précision des prédictions. Nous utilisons une répartition 80/20 pour diviser les données en ensemble d'entraînement et d'essai. Il faut donc prendre le temps de récupérer des données en quantité et qualité suffisantes, en évitant les biais de représentativité.

## 3 – Méthodologie

Dans cette étude, nous utiliserons plusieurs algorithmes d'apprentissage pour effectuer les prédictions. Les algorithmes choisis incluent :

- MLPClassifier : cet algorithme utilise un réseau de neurones multicouches pour effectuer la classification. Il peut traiter les relations complexes entre les différentes variables et est souvent utilisé pour les jeux de données dans lesquels les variables ont un impact significatif sur la variable cible.
- SVM (Support Vector Machine) : cet algorithme utilise une méthode de séparation pour faire la classification. Il est souvent utilisé pour les jeux de données dans lesquels les variables sont très corrélées.
- Régression logistique : cet algorithme effectue une régression en utilisant une fonction sigmoïde pour prédire la probabilité de l'occurrence d'un événement binaire. Il est souvent utilisé pour les problèmes de classification binaire.
- Random Forest : cet algorithme utilise une collection d'arbres de décision pour faire la classification. Il est souvent utilisé pour les jeux de données complexes avec de nombreuses variables.

Nous avons choisi ces algorithmes en raison de leur capacité à gérer les jeux de données complexes et leur capacité à faire des prédictions précises.

## 4 – Analyse des données

### I – Graphiques de données

## ![](resources/plot_number_of_patients_by_output.png)

On remarque ici que le nombre de patients présentant un risque d’avoir une crise cardiaque est supérieur au nombre de patient ne présentant pas de risque.

## ![](resources/plot_age_by_output.png)

Ce graphique nous montre l'âge des patients en fonction de leur état d'output. Les patients présentant un risque d’avoir une crise cardiaque sont plus âgés que les patients ne présentant pas de risque.

## ![](resources/plot_maximum_heart_rate_by_age.png)

Ce graphique nous montre la fréquence maximale atteinte par chaque patient en fonction de leur âge. On observe que la fréquence maximale est plus élevée chez les patients les plus jeunes et inversement moins élevé chez les patients plus âgés.

## ![](resources/plot_big.png)

Ce grand graphique nous expose beaucoup de données croisées.

Rappel des abréviations :

- trtbps = la tension artérielle à repos
- chol = le taux de cholestérol
- thalach = le nombre de battements cardiaques maximum au cours d'un exercice
- l'état d'output

**Le graphique numéro 1** compare la fréquence cardiaque maximale en fonction de la tension artérielle au repos. On remarque que les patients à risque procèdent une fréquence cardiaque maximale plus élevée que les patients ne présentant pas de risque. On remarque également que les patients ne présentant pas de risques possèdent une pression artérielle au repos sensiblement plus élevée.

**Le graphique numéro 2** compare le cholestérol en fonction du nombre de battements cardiaques maximum au cours d'un exercice. On remarque que les patients présentant un risque élevé de crise cardiaque possèdent un nombre de battements cardiaques maximum au cours d'un exercice significativement plus élevé.

**Le graphique numéro 3** compare la tension artérielle au repos en fonction du cholestérol. On remarque que les patients à risque procèdent une tension artérielle plus basse que les patients ne présentant pas de risque. Ils ont cependant significativement le même taux de cholestérol.

## ![](resources/plot_age_distribution_of_patients_depending_on_heart_desease.png)

Ce graphique nous montre la répartition par âge des patients en fonction du risque de maladie cardiaque. On remarque d’après ce graphique que les patients plus âgés développent significativement plus de risques d’être atteint de maladie cardiaque. Les patients âgés de 55 à 65 ans sont sensiblement des patients présentant des risques de développer des crises cardiaques.

## ![](resources/plot_distribution_of_BP_around_patients.png)

Ce graphique expose la distribution des patients en fonction de leur pression artérielle. On remarque que la plupart des patients possèdent une pression artérielle entre 120 et 140.

## II – Matrices de confusions

## ![](resources/confusion_SVM.png) ![](resources/confusion_logic_regression.png) ![](resources/confusion_random_forest.png) ![](resources/confusion_MLP.png)

Voici les matrices de confusions de nos modèles utilisant nos différents algorithmes.

<u>Comment les lire ?</u> (cf Matrice Random Forest)

Horizontalement, sur les 27 patients ne présentent pas de risques (cf. : 23+4), 23 ont été estimés par le système de classification comme tels et 4 ont été estimés comme à risque (cf : 4 faux négatifs).

Horizontalement, sur les 34 présentant des risques (cf. : 5+29), 5 ont été estimés comme ne présentent pas de risques (cf : 5 faux positifs) et 29 ont été estimés comme présentant des risques.

Verticalement, sur les 28 patients (cf. : 23+5) estimés par le système comme sans risque de faire une crise cardiaque, 5 sont en fait à risques de faire une crise cardiaque.

verticalement, sur les 33 mails (cf. : 29+4) estimés par le système comme à risques de faire une crise cardiaque, 4 sont en fait des patients ne présentant pas de risques de faire de crises cardiaques.

## 5 – Résultats

|          **Modèle**          | **Précision (%)** |
| :--------------------------: | :---------------: |
|      Logistic Regressor      |       85.25       |
| Support Vector Machine (SVM) |       86.89       |
|        MLPClassifier         |       85.25       |
|        Random Forest         |       88.52       |

Nous avons utilisé quatre algorithmes différents pour effectuer ces prédictions : la régression logistique (LR), le support vector machine (SVM), la forêt aléatoire (RF) et le réseau de neurones multicouches (NN).

L'analyse des données a révélé que l'algorithme de réseau de neurones multicouches a obtenu la plus grande précision de 88,52%, tandis que la régression logistique et la forêt aléatoire ont obtenu une précision de 85,24%. Le support vector machine (SVM) a obtenu une précision légèrement supérieure de 86,88%.

## 6 – Discussions

En général, ces résultats montrent que les algorithmes utilisés sont efficaces pour prédire le risque de crise cardiaque chez les différentes personnes. Toutefois, il est important de noter que ces prédictions sont soumises à une incertitude (≈15%) et ne doivent pas être considérées comme des diagnostics médicaux définitifs. Il est recommandé de consulter un médecin pour une évaluation complète de la santé cardiaque.

Les modèles d'apprentissage automatique utilisés dans cette étude (MLPclassifier, SVM, Régression logistique et Random Forest) ont tous leurs forces et leurs faiblesses.

### I – Forces des modèles

- MLPclassifier : la force de ce modèle est qu'il est capable de gérer les relations complexes entre les variables en utilisant des réseaux de neurones.
- SVM : la force de ce modèle est qu'il est capable de séparer les données linéairement séparables et de gérer les données non linéaires en utilisant des noyaux.
- Régression logistique : la force de ce modèle est qu'il est simple à implémenter et qu'il peut donner des probabilités pour les prédictions, ce qui peut être utile pour les applications médicales.
- Random Forest : la force de ce modèle est qu'il est capable de gérer les variables non linéaires et de limiter l'effet de sur-apprentissage en utilisant plusieurs arbres décisionnels.

### II – Faiblesses des modèles

- MLPclassifier : La faiblesse de ce modèle est qu'il peut être sujet à l'effet de sur-apprentissage, ce qui peut entraîner une faible précision des prédictions sur les données de test.
- SVM : La faiblesse de ce modèle est qu'il peut être lent à entraîner pour des jeux de données de grande taille en raison de sa complexité.
- Régression logistique : La faiblesse de ce modèle est qu'il ne peut gérer que les relations linéaires entre les variables.
- Random Forest : La faiblesse de ce modèle est qu'il peut être sensible aux bruits et à la corrélation des variables.

En fin de compte, le choix de l'algorithme dépendra de la structure des données et de la précision requise pour les prédictions. Il peut être nécessaire d'essayer plusieurs algorithmes pour déterminer celui qui donne les meilleurs résultats pour une application spécifique.
