import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score
from sklearn.neural_network import MLPClassifier


def MLP_classifier_model(x_train, y_train, x_test, y_test):
    model = MLPClassifier(random_state=1, max_iter=1000)
    model.fit(x_train, y_train)
    predicted = model.predict(x_test)
    conf = confusion_matrix(y_test, predicted)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf, display_labels=model.classes_)
    disp.plot()
    plt.title("MLP")
    plt.show()
    print("The Accuracy of NN is: ", accuracy_score(y_test, predicted) * 100)
